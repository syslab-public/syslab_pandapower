# SYSLAB_pandapower

## Description
Electrical model of the SYSLAB grid for pandapower.

## Installation
This will need the pandapower python package. The model has been tested with version 2.13.

## Usage
The model is divided into individual substation switchboards (syslab_switchboard_xxx_x.py) and the main interconnection cables (syslab_cables.py) in order to keep the complexity of a 164-node model manageable. 

By default, all circuit breakers of the model are open. In order to analyze a specific lab configuration, the corresponding circuit breakers should be closed (closed=True) in all relevant places.

The model can be loaded using
`
import syslab_grid_15082023
net=syslab_grid_15082023.create_syslab_model()
`

## Authors and acknowledgment
Oliver Gehrke (olge@dtu.dk), 2023

## License
BSD 3-clause

