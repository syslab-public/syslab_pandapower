import pandapower as pp

import syslab_substation_117_2
import syslab_substation_117_4
import syslab_substation_117_5
import syslab_substation_117_6
import syslab_substation_310_2
import syslab_substation_319_2
import syslab_substation_crossbar
import syslab_substation_330_12
import syslab_substation_330_13
import syslab_substation_715_2
import syslab_substation_716_2
import syslab_cables

def create_syslab_model():

  net = pp.create_empty_network()

  syslab_substation_117_2.add_117_2_to_net(net)
  syslab_substation_117_4.add_117_4_to_net(net)
  syslab_substation_117_5.add_117_5_to_net(net)
  syslab_substation_117_6.add_117_6_to_net(net)
  syslab_substation_310_2.add_310_2_to_net(net)
  syslab_substation_319_2.add_319_2_to_net(net)
  syslab_substation_crossbar.add_crossbar_to_net(net)
  syslab_substation_330_12.add_330_12_to_net(net)
  syslab_substation_330_13.add_330_13_to_net(net)
  syslab_substation_715_2.add_715_2_to_net(net)
  syslab_substation_716_2.add_716_2_to_net(net)

  syslab_cables.add_cables_to_net(net)

  return net