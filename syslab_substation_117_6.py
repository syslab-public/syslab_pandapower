import pandapower as pp

def add_117_6_to_net(net):
    # busbars
    bus_117_6A = pp.create_bus(net, name="117-6_Busbar-A", vn_kv=0.4, type="b")
    bus_117_6B = pp.create_bus(net, name="117-6_Busbar-B", vn_kv=0.4, type="b")
    #bay-level nodes
    node_117_6_EVSE_1 = pp.create_bus(net, name="117-6_EVSE_1", vn_kv=0.4, type="n")
    node_117_6_EVSE_2 = pp.create_bus(net, name="117-6_EVSE_2", vn_kv=0.4, type="n")
    node_117_6_EVSE_3 = pp.create_bus(net, name="117-6_EVSE_3", vn_kv=0.4, type="n")
    node_117_6_EVSE_4 = pp.create_bus(net, name="117-6_EVSE_4", vn_kv=0.4, type="n")
    node_117_6_EVSE_5 = pp.create_bus(net, name="117-6_EVSE_5", vn_kv=0.4, type="n")
    node_117_6_EVSE_6 = pp.create_bus(net, name="117-6_EVSE_6", vn_kv=0.4, type="n")
    node_117_6_EVSE_7 = pp.create_bus(net, name="117-6_EVSE_7", vn_kv=0.4, type="n")
    node_117_6_EVSE_8 = pp.create_bus(net, name="117-6_EVSE_8", vn_kv=0.4, type="n")
    #breakers
    bkr_117_6_EVSE_1_A = pp.create_switch(net, bus_117_6A, node_117_6_EVSE_1, et="b", type="CB", closed=False)
    bkr_117_6_EVSE_1_B = pp.create_switch(net, bus_117_6B, node_117_6_EVSE_1, et="b", type="CB", closed=False)
    bkr_117_6_EVSE_2_A = pp.create_switch(net, bus_117_6A, node_117_6_EVSE_2, et="b", type="CB", closed=False)
    bkr_117_6_EVSE_2_B = pp.create_switch(net, bus_117_6B, node_117_6_EVSE_2, et="b", type="CB", closed=False)
    bkr_117_6_EVSE_3_A = pp.create_switch(net, bus_117_6A, node_117_6_EVSE_3, et="b", type="CB", closed=False)
    bkr_117_6_EVSE_3_B = pp.create_switch(net, bus_117_6B, node_117_6_EVSE_3, et="b", type="CB", closed=False)
    bkr_117_6_EVSE_4_A = pp.create_switch(net, bus_117_6A, node_117_6_EVSE_4, et="b", type="CB", closed=False)
    bkr_117_6_EVSE_4_B = pp.create_switch(net, bus_117_6B, node_117_6_EVSE_4, et="b", type="CB", closed=False)
    bkr_117_6_EVSE_5_A = pp.create_switch(net, bus_117_6A, node_117_6_EVSE_5, et="b", type="CB", closed=False)
    bkr_117_6_EVSE_5_B = pp.create_switch(net, bus_117_6B, node_117_6_EVSE_5, et="b", type="CB", closed=False)
    bkr_117_6_EVSE_6_A = pp.create_switch(net, bus_117_6A, node_117_6_EVSE_6, et="b", type="CB", closed=False)
    bkr_117_6_EVSE_6_B = pp.create_switch(net, bus_117_6B, node_117_6_EVSE_6, et="b", type="CB", closed=False)
    bkr_117_6_EVSE_7_A = pp.create_switch(net, bus_117_6A, node_117_6_EVSE_7, et="b", type="CB", closed=False)
    bkr_117_6_EVSE_7_B = pp.create_switch(net, bus_117_6B, node_117_6_EVSE_7, et="b", type="CB", closed=False)
    bkr_117_6_EVSE_8_A = pp.create_switch(net, bus_117_6A, node_117_6_EVSE_8, et="b", type="CB", closed=False)
    bkr_117_6_EVSE_8_B = pp.create_switch(net, bus_117_6B, node_117_6_EVSE_8, et="b", type="CB", closed=False)