import pandapower as pp

def add_117_4_to_net(net):
    # busbars
    bus_117_4A = pp.create_bus(net, name="117-4_Busbar-A", vn_kv=0.4, type="b")
    #bay-level nodes
    node_117_4_Grid = pp.create_bus(net, name="117-4_Grid", vn_kv=0.4, type="n")
    node_117_4_Container_3 = pp.create_bus(net, name="117-4_Container_3", vn_kv=0.4, type="n")
    node_117_4_Cable_H1 = pp.create_bus(net, name="117-4_Cable_H1", vn_kv=0.4, type="n")
    #breakers
    bkr_117_4_Container_3_A = pp.create_switch(net, bus_117_4A, node_117_4_Container_3, et="b", type="CB", closed=False)
    bkr_117_4_Grid_A = pp.create_switch(net, bus_117_4A, node_117_4_Grid, et="b", type="CB", closed=False)
    bkr_117_4_Cable_H1_A = pp.create_switch(net, bus_117_4A, node_117_4_Cable_H1, et="b", type="CB", closed=False)
