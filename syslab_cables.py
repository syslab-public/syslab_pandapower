import pandapower as pp

def get_bus_index(net,bus_name):
    df=net.bus
    xl=df.index[df['name']==bus_name].tolist()
    if len(xl)>1:
        raise Exception('Bus "'+bus_name+'" is not unambiguous ('+str(len(xl))+' occurrences). Cannot connect.')
        exit(1)
    if len(xl)==0:
        raise Exception('Bus "'+bus_name+'" not found. Cannot connect.')
        exit(1)
    return xl[0]

def add_cables_to_net(net):
    #95 mm2 cables
    pp.create_std_type(net, {"r_ohm_per_km": 0.313, "x_ohm_per_km": 0.078, "c_nf_per_km": 350, "max_i_ka": 9.665, "r0_ohm_per_km": 1.283, "x0_ohm_per_km": 0.312,
                             "c0_nf_per_km": 0, "endtemp_degree": 70.0}, name="NOIK-AL-M 4x95 0.6/1kV", element="line") #TODO: check c0 value
    pp.create_std_type(net, {"r_ohm_per_km": 0.313, "x_ohm_per_km": 0.078, "c_nf_per_km": 350, "max_i_ka": 9.665, "r0_ohm_per_km": 1.283, "x0_ohm_per_km": 0.312,
                             "c0_nf_per_km": 0, "endtemp_degree": 70.0}, name="HIK-AL-S 4x95 0.6/1kV", element="line") #TODO: values copied from NOIK-AL-M, need to find datasheet
    pp.create_std_type(net, {"r_ohm_per_km": 0.313, "x_ohm_per_km": 0.078, "c_nf_per_km": 350, "max_i_ka": 9.665, "r0_ohm_per_km": 1.283, "x0_ohm_per_km": 0.312,
                             "c0_nf_per_km": 0, "endtemp_degree": 70.0}, name="HF-AL-S 4x95 0.6/1kV", element="line") #TODO: values copied from NOIK-AL-M, need to find datasheet, C values unknown
    pp.create_std_type(net, {"r_ohm_per_km": 0.313, "x_ohm_per_km": 0.078, "c_nf_per_km": 350, "max_i_ka": 9.665, "r0_ohm_per_km": 1.283, "x0_ohm_per_km": 0.312,
                             "c0_nf_per_km": 0, "endtemp_degree": 70.0}, name="DK-AQX-AL-S 4x95 0.6/1kV", element="line") #TODO: values copied from HF-AL-S, need to find datasheet, C values unknown
    #150 mm2 cables
    pp.create_std_type(net, {"r_ohm_per_km": 0.202, "x_ohm_per_km": 0.078, "c_nf_per_km": 350, "max_i_ka": 9.665, "r0_ohm_per_km": 0.832, "x0_ohm_per_km": 0.305,
                             "c0_nf_per_km": 0, "endtemp_degree": 70.0}, name="NOIK-AL-S 4x150 0.6/1kV", element="line") #TODO: check c and ik values
    #240 mm2 cables
    pp.create_std_type(net, {"r_ohm_per_km": 0.122, "x_ohm_per_km": 0.077, "c_nf_per_km": 350, "max_i_ka": 9.665, "r0_ohm_per_km": 0.3556, "x0_ohm_per_km": 0.217,
                             "c0_nf_per_km": 0, "endtemp_degree": 70.0}, name="HF-AL-S 4x240 0.6/1kV", element="line") #TODO: values from old cables, need checking
    pp.create_std_type(net, {"r_ohm_per_km": 0.122, "x_ohm_per_km": 0.077, "c_nf_per_km": 350, "max_i_ka": 9.665, "r0_ohm_per_km": 0.3556, "x0_ohm_per_km": 0.217,
                             "c0_nf_per_km": 0, "endtemp_degree": 70.0}, name="DK-AQX-AL-S 4x240 0.6/1kV", element="line") #TODO: values copied from HF-AL-S, need to find datasheet

    pp.create_line(net, get_bus_index(net,'319-2_Cable_A1A'), get_bus_index(net,'319-3_Cable_A1A'), length_km=0.021, std_type="NOIK-AL-M 4x95 0.6/1kV", name="Cable A1A")
    pp.create_line(net, get_bus_index(net,'319-2_Cable_A1B'), get_bus_index(net,'319-3_Cable_A1B'), length_km=0.021, std_type="NOIK-AL-M 4x95 0.6/1kV", name="Cable A1B")
    pp.create_line(net, get_bus_index(net,'319-2_Cable_A2A'), get_bus_index(net,'319-3_Cable_A2A'), length_km=0.021, std_type="HIK-AL-S 4x95 0.6/1kV", name="Cable A2A")
    pp.create_line(net, get_bus_index(net,'319-2_Cable_A2B'), get_bus_index(net,'319-3_Cable_A2B'), length_km=0.021, std_type="HIK-AL-S 4x95 0.6/1kV", name="Cable A2B")
    pp.create_line(net, get_bus_index(net,'715-2_Cable_B1A'), get_bus_index(net,'319-3_Cable_B1A'), length_km=0.188, std_type="NOIK-AL-M 4x95 0.6/1kV", name="Cable B1A")
    pp.create_line(net, get_bus_index(net,'715-2_Cable_B1B'), get_bus_index(net,'319-3_Cable_B1B'), length_km=0.188, std_type="NOIK-AL-M 4x95 0.6/1kV", name="Cable B1B")
    pp.create_line(net, get_bus_index(net,'715-2_Cable_B2A'), get_bus_index(net,'319-3_Cable_B2A'), length_km=0.176, std_type="HF-AL-S 4x95 0.6/1kV", name="Cable B2A")
    pp.create_line(net, get_bus_index(net,'715-2_Cable_B2B'), get_bus_index(net,'319-3_Cable_B2B'), length_km=0.176, std_type="HF-AL-S 4x95 0.6/1kV", name="Cable B2B")
    pp.create_line(net, get_bus_index(net,'117-2_Cable_C1A'), get_bus_index(net,'319-3_Cable_C1A'), length_km=0.690, std_type="HF-AL-S 4x240 0.6/1kV", name="Cable C1A")
    pp.create_line(net, get_bus_index(net,'117-2_Cable_C1B'), get_bus_index(net,'319-3_Cable_C1B'), length_km=0.690, std_type="HF-AL-S 4x240 0.6/1kV", name="Cable C1B")
    pp.create_line(net, get_bus_index(net,'117-2_Cable_C2A'), get_bus_index(net,'319-3_Cable_C2A'), length_km=0.690, std_type="HF-AL-S 4x240 0.6/1kV", name="Cable C2A")
    pp.create_line(net, get_bus_index(net,'117-2_Cable_C2B'), get_bus_index(net,'319-3_Cable_C2B'), length_km=0.690, std_type="HF-AL-S 4x240 0.6/1kV", name="Cable C2B")
    pp.create_line(net, get_bus_index(net,'117-4_Busbar-A'), get_bus_index(net, '117-5_Cable_D1'), length_km=0.005,std_type="DK-AQX-AL-S 4x240 0.6/1kV", name="Cable D1")
    pp.create_line(net, get_bus_index(net,'716-2_Cable_E1A'), get_bus_index(net,'319-3_Cable_E1A'), length_km=0.411, std_type="HF-AL-S 4x240 0.6/1kV", name="Cable E1A")
    pp.create_line(net, get_bus_index(net,'716-2_Cable_E1B'), get_bus_index(net,'319-3_Cable_E1B'), length_km=0.411, std_type="HF-AL-S 4x240 0.6/1kV", name="Cable E1B")
    pp.create_line(net, get_bus_index(net,'716-2_Cable_E2A'), get_bus_index(net,'319-3_Cable_E2A'), length_km=0.411, std_type="HF-AL-S 4x240 0.6/1kV", name="Cable E2A")
    pp.create_line(net, get_bus_index(net,'716-2_Cable_E2B'), get_bus_index(net,'319-3_Cable_E2B'), length_km=0.411, std_type="HF-AL-S 4x240 0.6/1kV", name="Cable E2B")
    pp.create_line(net, get_bus_index(net,'716-2_Cable_F1'), get_bus_index(net, '715-2_Cable_F1'), length_km=0.250,std_type="NOIK-AL-S 4x150 0.6/1kV", name="Cable F1")
    pp.create_line(net, get_bus_index(net,'117-6_Busbar-A'), get_bus_index(net, '117-5_Cable_G1'), length_km=0.011,std_type="DK-AQX-AL-S 4x95 0.6/1kV", name="Cable G1")
    pp.create_line(net, get_bus_index(net,'117-6_Busbar-B'), get_bus_index(net, '117-4_Cable_H1'), length_km=0.014,std_type="DK-AQX-AL-S 4x95 0.6/1kV", name="Cable H1")
    pp.create_line(net, get_bus_index(net,'310-2_Cable_J1A'), get_bus_index(net,'319-4_Cable_J1A'), length_km=0.068, std_type="HF-AL-S 4x240 0.6/1kV", name="Cable J1A")
    pp.create_line(net, get_bus_index(net,'310-2_Cable_J1B'), get_bus_index(net,'319-4_Cable_J1B'), length_km=0.068, std_type="HF-AL-S 4x240 0.6/1kV", name="Cable J1B")
    pp.create_line(net, get_bus_index(net,'310-2_Cable_J2A'), get_bus_index(net,'319-4_Cable_J2A'), length_km=0.068, std_type="HF-AL-S 4x240 0.6/1kV", name="Cable J2A")
    pp.create_line(net, get_bus_index(net,'310-2_Cable_J2B'), get_bus_index(net,'319-4_Cable_J2B'), length_km=0.068, std_type="HF-AL-S 4x240 0.6/1kV", name="Cable J2B")
    pp.create_line(net, get_bus_index(net,'330-12_Cable_K1A'), get_bus_index(net,'319-4_Cable_K1A'), length_km=0.267, std_type="HF-AL-S 4x240 0.6/1kV", name="Cable K1A")
    pp.create_line(net, get_bus_index(net,'330-12_Cable_K1B'), get_bus_index(net,'319-4_Cable_K1B'), length_km=0.267, std_type="HF-AL-S 4x240 0.6/1kV", name="Cable K1B")
    pp.create_line(net, get_bus_index(net,'330-12_Cable_K2A'), get_bus_index(net,'319-4_Cable_K2A'), length_km=0.267, std_type="HF-AL-S 4x240 0.6/1kV", name="Cable K2A")
    pp.create_line(net, get_bus_index(net,'330-12_Cable_K2B'), get_bus_index(net,'319-4_Cable_K2B'), length_km=0.267, std_type="HF-AL-S 4x240 0.6/1kV", name="Cable K2B")
    pp.create_line(net, get_bus_index(net,'330-13_Cable_L1'), get_bus_index(net,'330-12_Cable_L1'), length_km=0.005, std_type="HF-AL-S 4x240 0.6/1kV", name="Cable L1")
    pp.create_line(net, get_bus_index(net,'117-2_Cable_X1'), get_bus_index(net,'117-5_Cable_X1'), length_km=0.024, std_type="NOIK-AL-M 4x95 0.6/1kV", name="Cable X1A")
    pp.create_line(net, get_bus_index(net,'117-2_Cable_X1'), get_bus_index(net,'117-5_Cable_X1'), length_km=0.024, std_type="NOIK-AL-M 4x95 0.6/1kV", name="Cable X1B")
    pp.create_line(net, get_bus_index(net,'117-2_Cable_X2'), get_bus_index(net,'117-5_Cable_X2'), length_km=0.024, std_type="NOIK-AL-M 4x95 0.6/1kV", name="Cable X2A")
    pp.create_line(net, get_bus_index(net,'117-2_Cable_X2'), get_bus_index(net,'117-5_Cable_X2'), length_km=0.024, std_type="NOIK-AL-M 4x95 0.6/1kV", name="Cable X2B")
    pp.create_line(net, get_bus_index(net, '319-3_Grid'), get_bus_index(net, '319-3_Busbar-A'), length_km=0.005, std_type="HF-AL-S 4x240 0.6/1kV", name="Grid 319-3")